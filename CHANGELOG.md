# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.8] - 2020-01-26
### Changed
- `swiftlibs debuginfo` now specifies if development mode is enabled

### Fixed
- `swiftlibs debuginfo` raising an unhandled error

## [0.0.7] - 2020-01-16
### Fixed
- Correctly include locales data in install

## [0.0.6] - 2020-01-11
### Changed
- Replaced `setup.cfg` with `setup.py`; this should (finally) fix package install issues

## [0.0.5] - 2020-01-10
### Added
- Anti-spam check utility (optional replacement for discord.py's built-in cooldown checks)
- `[p]swiftlibs debug time` now returns both the full relative time and what the given delta resolves to in a datetime format
- `[p]swiftlibs debuginfo` now lists the Config driver in use
- Migration versions may now be forced to always run; this is primarily intended to be used for debugging migrations.

### Changed
- Completely rewrote how FutureTime internally parses time deltas
- **[BREAKING]** Migrations now requires a Config object to be passed instead of a cog name upon creation
    - Additionally, migrations now store the cog's migration status in the cog's Config.
- Menu utilities now check to see if we're actually allowed to perform a cleanup action beforehand more often
- Menus will now preemptively raise an error upon subclassing if your menu goes over 20 actions

### Deprecated
- `FutureTime#default_period.period()` is now deprecated in favour of `FutureTime#default_period("period")`
- `FutureTime#{min,max}_time.period(...)` is now deprecated in favour of `FutureTime#{min,max}_time(period=...)`
- `FutureTime.get_seconds()` is now deprecated in favour of `TimeRepresentation.parse()`

### Removed
- `FutureTime#strict()`
- `Translator#sub()`
- `Translator#babel`
- `swift_libs.objs`
- `swift_libs.setup.swift_libs.uptime`

### Fixed
- Help docs for `[p]swiftlibs debug` commands now properly bypass translation
- Correctly include `swift_libs.menus` and `swift_libs.time` when installing

## [0.0.4] - 2019-11-30
### Fixed
- Package failing to install

## [0.0.3] - 2019-11-27
### Added
- Convert swift_libs to a Python package instead of being shipped as a shared library

[Unreleased]: https://gitlab.com/odinair/swift_libs/compare/v0.0.8...HEAD
[0.0.8]: https://gitlab.com/odinair/swift_libs/compare/v0.0.7...v0.0.8
[0.0.7]: https://gitlab.com/odinair/swift_libs/compare/v0.0.6...v0.0.7
[0.0.6]: https://gitlab.com/odinair/swift_libs/compare/v0.0.5...v0.0.6
[0.0.5]: https://gitlab.com/odinair/swift_libs/compare/v0.0.4...v0.0.5
[0.0.4]: https://gitlab.com/odinair/swift_libs/compare/v0.0.3...v0.0.4
[0.0.3]: https://gitlab.com/odinair/swift_libs/-/tags/v0.0.3

