from __future__ import annotations

import re
from dataclasses import dataclass
from datetime import datetime, timedelta
from enum import Enum
from functools import wraps
from math import floor
from typing import Union, List, Optional, Callable

from .utils import _month_delta, _year_delta

__all__ = ("AboveMaxDuration", "BelowMinDuration", "NoDuration", "TimeRepresentation", "TimePeriod")

TIME_AMNT_REGEX = re.compile(
    r"(?P<AMOUNT>[0-9]+(?:\.[0-9]+)?)\s?(?P<PERIOD>[a-z]+)?", re.IGNORECASE
)


class BelowMinDuration(ValueError):
    """Raised if an input resolves to be below a converter's minimum required duration"""


class AboveMaxDuration(ValueError):
    """Raised if an input resolves to be above the converter's max allowed duration"""


class NoDuration(ValueError):
    """Raised if an input fails to resolve to any meaningful timedelta"""


def _wrap(wrap):
    @wraps(wrap)
    def wrapper(now: datetime, quantity: Union[int, float]):
        quantity = float(quantity)
        dec, quantity = quantity % 1, floor(quantity)
        delta = 0.0
        for i in range(quantity):
            delta += wrap(now + timedelta(seconds=delta))
        if dec:
            delta += wrap(now + timedelta(seconds=delta)) * dec
        return delta

    return wrapper


class TimePeriod(Enum):
    SECONDS = ("seconds", 1.0)
    MINUTES = ("minutes", 60.0)
    HOURS = ("hours", MINUTES[1] * 60.0)
    DAYS = ("days", HOURS[1] * 24.0)
    WEEKS = ("weeks", DAYS[1] * 7.0)
    MONTHS = ("months", _wrap(_month_delta))
    YEARS = ("years", _wrap(_year_delta))

    def __init__(self, suffix: str, time_period: Union[float, Callable[[datetime, float], float]]):
        self.suffix = suffix
        self.time_period = time_period

    def __call__(self, now: datetime, quantity: Union[int, float]) -> float:
        if callable(self.time_period):
            delta = self.time_period(now, quantity)
        else:
            delta = self.time_period * quantity
        return delta

    @classmethod
    def find(cls, period: str) -> Optional[TimePeriod]:
        try:
            return next(x for x in cls if x.suffix.startswith(period.lower()))
        except StopIteration:
            return None


@dataclass(frozen=True, eq=False)
class TimeRepresentation:
    amount: Union[int, float]
    period: TimePeriod

    def to_seconds(self, now: datetime = ...) -> float:
        now = now or datetime.utcnow()
        return self.period(now, self.amount)

    @classmethod
    def full_parse(
        cls, reps: List[TimeRepresentation], now: datetime = None, max_duration: timedelta = None
    ) -> timedelta:
        if now is None:
            now = datetime.utcnow()

        delta = timedelta()
        for rep in reps:
            # delta += timedelta(seconds=rep.period(now + delta, rep.amount))
            delta += timedelta(seconds=rep.to_seconds(now + delta))
            if max_duration and delta > max_duration:
                raise AboveMaxDuration()

        return delta

    @classmethod
    def parse(
        cls,
        duration: str,
        *,
        max_duration: Union[timedelta, List[TimeRepresentation]] = None,
        default: TimePeriod = None
    ) -> timedelta:
        if isinstance(max_duration, list):
            max_duration = cls.full_parse(max_duration)

        reps = []
        for match in TIME_AMNT_REGEX.finditer(duration):
            try:
                amount = float(match.group("AMOUNT"))
            except ValueError:
                continue
            period = match.group("PERIOD")
            try:
                period = next(x for x in TimePeriod if x.suffix.startswith(period.lower()))
            except (StopIteration, AttributeError):
                if default is None:
                    continue
                period = default
            reps.append(cls(amount=amount, period=period))
        return cls.full_parse(reps, max_duration=max_duration)
