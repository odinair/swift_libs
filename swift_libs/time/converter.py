from __future__ import annotations

import warnings
from copy import deepcopy
from dataclasses import dataclass
from datetime import timedelta
from typing import Union, Optional, Dict, Any, List

from babel.dates import format_timedelta
from discord.ext.commands import BadArgument, Converter
from redbot.core.utils.chat_formatting import escape

from .._internal import translate
from .parser import BelowMinDuration, AboveMaxDuration, NoDuration, TimeRepresentation, TimePeriod

__all__ = ("FutureTime", "ConfigKey", "BelowMinDuration", "AboveMaxDuration", "NoDuration")


class FutureTime(Converter):
    # noinspection PyUnresolvedReferences
    """
    Time delta conversion utility.

    This class is originally adapted from ZeLarpMaster's Red v2 Reminders cog, but has since
    been heavily rewritten from the `original source`_.

    .. _original source: https://github.com/ZeLarpMaster/ZeCogs/blob/53ee681/reminder/reminder.py

    Example
    -------

    >>> from swift_libs.time import FutureTime
    >>> converter = FutureTime().min_time(seconds=10)

    You may also use this class as a type hint if you'd like discord.py to handle the
    grunt work of conversions for you::

        >>> from swift_libs.time import FutureTime
        >>> from redbot.core import commands
        >>> @commands.command()
        ... async def my_cmd(ctx, duration: FutureTime().min_time(seconds=10)):
        ...     # duration will be an instance of datetime.timedelta
        ...     # invalid input for duration will result in a BadArgument exception,
        ...     # which is handled by discord.py like any other conversion failure
        ...     await ctx.send(f"Total seconds: {duration.total_seconds()}")
    """

    def __init__(self, **kwargs):
        self._original_kwargs: Dict[str, Any] = kwargs.copy()
        self._min_time: List[TimeRepresentation] = kwargs.pop("min_time", [])
        self._max_time: List[TimeRepresentation] = kwargs.pop("max_time", [])
        self._default_period: Optional[TimePeriod] = kwargs.pop("default_period", None)
        if kwargs:
            raise TypeError(f"Got unexpected keyword argument {next(iter(kwargs.keys()))!r}")

    def __repr__(self):
        return "{}({})".format(
            type(self).__name__, ", ".join([f"{k}={v!r}" for k, v in self._original_kwargs.items()])
        )

    def from_str(self, duration: str) -> timedelta:
        """Resolve an input duration with the converter's current configuration options

        All exceptions this method raises are subclasses of :class:`ValueError`.

        Raises
        -------
        BelowMinDuration
        AboveMaxDuration
        NoDuration
        """
        duration = TimeRepresentation.parse(
            duration, max_duration=self._max_time, default=self._default_period
        )
        if self._min_time:
            mint = TimeRepresentation.full_parse(self._min_time)
            if duration < mint:
                raise BelowMinDuration()
        if duration.total_seconds() == 0.0:
            raise NoDuration()
        return duration

    @staticmethod
    def get_seconds(
        duration: str,
        *,
        default_period: Optional[str] = None,
        max_seconds: Union[int, float] = None,
        **_,
    ) -> float:
        """Resolve a given string into it's representation in seconds

        .. deprecated::
            This is deprecated in favour of :meth:`swift_libs.time.parser.TimeRepresentation.parse`.
        """
        warnings.warn(
            "FutureTime.get_seconds() is deprecated; use TimeRepresentation.parse() instead",
            DeprecationWarning,
        )

        return TimeRepresentation.parse(
            duration,
            default=TimePeriod.find(default_period),
            max_duration=timedelta(seconds=max_seconds),
        ).total_seconds()

    async def convert(self, ctx, argument: str) -> timedelta:
        # this doc string is intentionally left empty; use `from_str` instead
        # of directly calling this method.
        """"""
        try:
            return self.from_str(argument)
        except NoDuration:
            raise BadArgument(
                translate(
                    "time.fail_parse_delta",
                    input=escape(argument, mass_mentions=True, formatting=True),
                )
            )
        except BelowMinDuration:
            raise BadArgument(
                translate(
                    "time.min_duration",
                    delta=format_timedelta(TimeRepresentation.full_parse(self._min_time)),
                )
            )
        except AboveMaxDuration:
            raise BadArgument(
                translate(
                    "time.max_duration",
                    delta=format_timedelta(TimeRepresentation.full_parse(self._max_time)),
                )
            )

    ###########

    @property
    def default_period(self) -> DefaultPeriod:
        """Configure the converter's default time period"""
        return DefaultPeriod(self._original_kwargs)

    @property
    def min_time(self) -> ConfigKey:
        """Configure the converter's minimum time required"""
        return ConfigKey("min_time", self._original_kwargs)

    @property
    def max_time(self) -> ConfigKey:
        """Configure the converter's maximum allowed time"""
        return ConfigKey("max_time", self._original_kwargs)


@dataclass()
class DefaultPeriod:
    kwargs: dict

    def __call__(self, period: str):
        default = TimePeriod.find(period)
        if default is None:
            raise KeyError(period)
        return FutureTime(**self.kwargs, default_period=default)

    # noinspection PyDeprecation
    def __getattr__(self, period: str):
        default = TimePeriod.find(period)
        if default is None:
            raise AttributeError(period)

        def partial():
            warnings.warn(
                f"FutureTime().default_period.{period}() is deprecated; use"
                f" FutureTime().default_period({period!r}) instead",
                DeprecationWarning,
            )
            return FutureTime(**self.kwargs, default_period=default)

        return partial


@dataclass()
class ConfigKey:
    """FutureTime configuration class

    This class is designed to be used to build a configured FutureTime in a fluent-style fashion.
    """

    key: str
    kwargs: dict

    def _copy(self):
        return deepcopy(self.kwargs)

    def __call__(self, *args, **kwargs):
        periods: Dict[TimePeriod, Union[int, float]] = {}
        seconds: float = sum((self.kwargs.get(self.key, 0.0), *args))
        for k, v in kwargs.items():
            period = TimePeriod.find(k)
            if not period:
                continue
            if period in periods:
                periods[period] += v
            else:
                periods[period] = v
        if periods:
            if TimePeriod.SECONDS not in periods:
                periods[TimePeriod.SECONDS] = 0
            periods[TimePeriod.SECONDS] += seconds
        reps = [TimeRepresentation(amount=v, period=k) for k, v in periods.items()]
        return self.append(reps)

    def append(self, value: list) -> FutureTime:
        kwargs = self._copy()
        if self.key not in kwargs:
            kwargs[self.key] = []
        kwargs[self.key].extend(value)
        return FutureTime(**kwargs)

    # noinspection PyDeprecation
    def __getattr__(self, key):
        period = TimePeriod.find(key)
        if not period:
            raise AttributeError(key)

        def partial(arg):
            warnings.warn(
                f"ConfigKey.{key}(...) is deprecated; use ConfigKey({key}=...)", DeprecationWarning
            )
            return self(**{key: arg})

        partial.__bool__ = lambda *_: False
        return partial
