__version__ = "0.0.8a0"

try:
    from redbot.core import VersionInfo

    version_info = VersionInfo.from_str(__version__)
except ImportError:
    # Likely being installed in an environment without Red already installed.
    version_info = __version__
