"""
Simple menu utility

This is designed to be something you can drop into any discord.py bot and use.
"""

# pylint:disable=wildcard-import
from .menu import *
from .paginate import *
from .wrappers import *
