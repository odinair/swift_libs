from __future__ import annotations

import asyncio
import logging
from contextlib import suppress
from dataclasses import dataclass
from typing import Callable, Optional, Awaitable, List

from redbot.core import Config

# noinspection PyProtectedMember
from redbot.core.config import Group

__all__ = ("Schema",)
log = logging.getLogger("red.swift_libs.migrations")


class Schema:
    """Cog migrations class

    This internally versions your cog with a version number based on either the highest version
    you've given a migration, or the total amount of migrations you've created for this schema.

    Example
    ------
    >>> from redbot.core import Config
    >>> config = Config.get_config(...)
    >>> schema = Schema(config)
    >>> # The following will be counted as migration 1. You can override the migration
    >>> # version number with the `version` keyword argument.
    >>> @schema.migration("example migration")
    ... async def example():
    ...     await config.a.set(config.b())
    ...     await config.b.clear()
    ...
    >>> async def setup(bot):
    ...     cog = ...
    ...     await schema.run()
    ...     # ...

    Arguments
    ---------
    config: Config
        Your cog's Config object; a ``SWIFTLIBS_MIGRATIONS`` scope will be registered for the
        purposes of storing your cog's known schema version alongside your cog data.
    name: str
        Optional name to use in logging regarding migrations. If this is not specified, this is
        retrieved from your specified Config.
    skip_first_run: bool
        If this is :obj:`False`, all migrations will be ran even if the cog is being loaded for
        the first time after migrations were added.

        .. important::
            If you have plans to use migrations in the future, you should mark your cog
            with :meth:`mark` as early as possible in order to prevent issues.
    """

    __slots__ = ("skip_first_run", "migrations", "config", "name")

    def __init__(self, config: Config, name: str = None, skip_first_run: bool = True):
        with suppress(ValueError):
            config.init_custom("SWIFTLIBS_MIGRATIONS", 0)
            config.register_custom("SWIFTLIBS_MIGRATIONS", version=None)
        self.config = config
        self.name = config.cog_name if name is None else name
        self.skip_first_run = skip_first_run
        self.migrations: List[Migration] = []

    def __repr__(self):
        return (
            f"<Schema cog_name={self.name!r} config={self.config!r} version={self.version!r}"
            f" migrations={self.migrations!r}>"
        )

    @property
    def _group(self) -> Group:
        return self.config.custom("SWIFTLIBS_MIGRATIONS")

    @property
    def version(self) -> int:
        """int: The current schema version. This is **not** the version stored in Config."""
        return max([*[x.version for x in self.migrations], 0])

    async def mark(self) -> None:
        """Mark the current cog as installed by setting its known schema version to 0

        This is primarily useful if you intend to possibly use migrations in the future,
        but currently have nothing to be migrated.
        """
        if await self._group.version() is None:
            log.debug("Marking cog %s as at migration 0.", self.name)
            await self._group.version.set(0)

    def migration(self, description: str = None, **kwargs) -> Callable[[Callable], Migration]:
        """Create a schema migration

        Keyword Arguments
        -----------------
        version: Optional[int]
            Optional version number to give this migration; defaults to ``Schema.version + 1``
        description: Optional[str]
            Optional description to give this schema. Used in logging when your cog's config schema
            is updated.
        silent: bool
            If this is :obj:`True`, this migration will not log anything when invoked.
        forced: bool
            If this is :obj:`True`, this migration will *always* be ran, even if the cog's schema
            version is equal to or greater than the version this migration is marked as.
            This is intended to only be used for debugging migrations.
        """
        kwargs.setdefault("version", self.version + 1)
        kwargs.setdefault("description", description)

        def wrapper(func):
            migration = Migration(callable=func, **kwargs)
            self.migrations.append(migration)
            return migration

        return wrapper

    async def run(self, *args, **kwargs):
        """Run applicable schema migrations"""
        ver = await self._group.version()
        if ver is None:
            await self._group.version.set(self.version)
            if self.skip_first_run:
                log.debug("Skipping first migration invoke for cog %s", self.name)
                return
            ver = 0
        if ver >= self.version and not any(x.forced for x in self.migrations):
            log.debug(
                "%s config is at schema version %s and we're at %s, nothing to do.",
                self.name,
                ver,
                self.version,
            )
            return

        for update in sorted(
            {*range(ver, self.version), *[x for x in self.migrations if x.forced]},
            key=lambda x: x.version if isinstance(x, Migration) else x,
        ):
            migration = self.migrations[update] if not isinstance(update, Migration) else update
            if not migration.silent:
                log.info(
                    "Updating schema for cog %s to version %s (%s)",
                    self.name,
                    migration.version,
                    migration.description or "no description provided",
                )
            try:
                await migration.callable(*args, **kwargs)
            except Exception as e:  # pylint:disable=broad-except
                log.exception("Failed to run migration for schema version %s", update, exc_info=e)
            else:
                if migration.version >= ver:
                    await self._group.version.set(migration.version)
                    ver = migration.version
            await asyncio.sleep(0)


@dataclass(frozen=True, eq=False)
class Migration:
    callable: Callable[..., Awaitable[None]]
    version: int
    description: Optional[str] = None
    silent: bool = False
    forced: bool = False

    def __hash__(self):
        return self.version

    def __call__(self, *_, **__):
        raise RuntimeError(
            "Migrations are not callable directly, but must instead be ran"
            " by using `Schema.run()`."
        )

    def __eq__(self, other):
        return (
            isinstance(other, type(self))
            and self.callable is other.callable
            and self.version == other.version
        )
