from os import environ as _environ

try:
    from redbot.core.config import Config as _Config
except ImportError:
    _Config = None

__all__ = ("undefined", "MONGODB")

#: This will be :obj:`True` if the MongoDB Config driver is currently in use, :obj:`False`
#: otherwise. This will always be :obj:`False` on Red 3.2 and greater.
MONGODB: bool = ...

# Deprecated constants; kept for backwards compatibility
NEW_MONGODB = True


if "BUILDING_DOCS" not in _environ and _Config is not None:
    try:
        # noinspection PyUnresolvedReferences
        from redbot.core.drivers.mongo import MongoDriver
        from ._internal import config

        MONGODB = isinstance(config.driver, MongoDriver)
        del config, MongoDriver
    except (ImportError, RuntimeError, TypeError):
        # we're either running in a headless environment such as a repl or in scripts,
        # or we're on Red 3.2+
        MONGODB = False


# noinspection PyPep8Naming
class undefined:
    def __repr__(self):
        return f"<{self.__module__}.undefined>"

    def __bool__(self):
        return False


#: Sentinel placeholder object to use where using :obj:`None` as a default would be ambiguous.
undefined = undefined()
