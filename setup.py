from distutils.core import setup
from setuptools import find_packages

from swift_libs import __version__

setup(
    name="swift_libs",
    description="Shared library for all of my Red Discord bot cogs",
    version=__version__,
    author="odinair",
    author_email="hi@odinair.xyz",
    url="https://gitlab.com/odinair/swift_libs",
    license="MIT",
    packages=find_packages(),
    python_requires=">=3.7",
    install_requires=["tzlocal"],
    extras_requires={"docs": ["sphinx"], "style": ["black==19.3b0"], "test": ["pylint"]},
    package_data={"swift_libs": ["locales/*.toml"]},
)
