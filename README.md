# swift_libs

Dependency for all of my [Red cogs](https://gitlab.com/odinair/Swift-Cogs).

## Setup

**This package assumes you already have [Red](https://github.com/Cog-Creators/Red-DiscordBot) installed and setup,**
and as such does not explicitly mark any dependencies for Red.

```sh
# Installing Red from git is recommended if you're setting up a development environment
$ pip install red-discordbot
$ pip install --extra-index-url=https://py.odinair.xyz/ swift_i18n
# This package is also available from the above index
$ pip install .
```

## To-Do

- Setup documentation
- Publish on PyPI?
